package com.gojek.parking.executor;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * For input type - > Interactive
 * InteractiveParkingLotProcessor class implements the process method extended from
 * AbstractProcessor class that get the command through cli and perform accordingly.
 */

public class InteractiveParkingLotProcessor extends AbstractProcessor {

    public void process() throws Exception {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));


        while(true) {
            String inputString = bufferRead.readLine();
            validateAndProcess(inputString);               // call validateAndProcess method for a line/command
        }
    }

}
