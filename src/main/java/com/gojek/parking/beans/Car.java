package com.gojek.parking.beans;

public class Car {
    String regNo;
    String color;

    public Car(String regNo, String color) {
        this.regNo = regNo;
        this.color = color;
    }

    //getRegNo() method to return registration number of car
    public String getRegNo() {
        return regNo;
    }

    //setRegNo() method to set registration number of a car
    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    //getColor() method to get color of a car
    public String getColor() {
        return color;
    }

    //setColor() method to set color of a car
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car [regNo=" + regNo + ", color=" + color + "]";
    }
}
