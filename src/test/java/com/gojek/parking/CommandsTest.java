package com.gojek.parking;

import static org.junit.Assert.assertTrue;

import com.gojek.parking.constants.Command;
import org.junit.Test;

public class CommandsTest {

    @Test
    public void testCommandNegative() throws Exception {
        Command command = Command.findByName("create_parking_lot----null");
        assertTrue(command == null);
    }


    @Test
    public void testCommandPositive() throws Exception {
        Command command = Command.findByName("create_parking_lot");
        assertTrue(command != null);
    }

}
