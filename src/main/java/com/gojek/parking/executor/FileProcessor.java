package com.gojek.parking.executor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * For input type -> File
 * FileProcessor class implements the process method extended from AbstractProcessor class
 * that get the filepath and read lines in fie one by one and perform accordingly.
 */

public class FileProcessor extends AbstractProcessor {

    String filePath = null;

    public FileProcessor(String filePath) {
        this.filePath = filePath;
    }

    public void process() throws Exception {
        File inputFile = new File(filePath);
        @SuppressWarnings("resource")
        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        String line;
        while ((line = br.readLine()) != null) {          // While loop to get each line of file.
            AbstractProcessor.validateAndProcess(line);   // call validateAndProcess method for each line in file
        }
    }

}
