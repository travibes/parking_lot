package com.gojek.parking;

import com.gojek.parking.executor.AbstractProcessor;
import com.gojek.parking.executor.FileProcessor;
import com.gojek.parking.executor.InteractiveParkingLotProcessor;

/**
 * Main class implements the main method that get either the file name a argument
 * or through standard input.
 */

public class Main {

    public static void main(String[] args) throws Exception {
        AbstractProcessor processor = null;

        if(args.length >= 1) {
            processor = new FileProcessor(args[0]);            // For input type File
        } else {
            processor = new InteractiveParkingLotProcessor();  // For input type interactive shell
        }
        processor.process();
    }
}
